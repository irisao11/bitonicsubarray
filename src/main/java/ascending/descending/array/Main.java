package ascending.descending.array;

class Main
{
    // Function to find length of Longest Bitonic Subarray in an array
    public static void findBitonicSubarray(int[] myArray)
    {
        // IncreasingArr[i] stores the length of the longest increasing sub-array
        // ending at myArray[i]

        int[] increasingSubArray = new int[myArray.length];
        increasingSubArray[0] = 1;
        for (int i = 1; i < myArray.length; i++) {
            increasingSubArray[i] = 1;
            if (myArray[i - 1] < myArray[i]) {
                increasingSubArray[i] = increasingSubArray[i - 1] + 1;
            }
        }

        // decreasingSubArray[i] stores the length of the longest decreasing sub-array
        // starting with myArray[i]
        int[] decreasingSubArray = new int[myArray.length];
        decreasingSubArray[myArray.length - 1] = 1;
        for (int i = myArray.length - 2; i >= 0; i--) {
            decreasingSubArray[i] = 1;
            if (myArray[i] > myArray[i + 1]) {
                decreasingSubArray[i] = decreasingSubArray[i + 1] + 1;
            }
        }

        // consider each element as peak and calculate longest subarray
        int subarrayLength = 1;
        int beginIndex = 0, endIndex = 0;

        for (int i = 0; i < myArray.length; i++)
        {
            if (subarrayLength < increasingSubArray[i] + decreasingSubArray[i] - 1)
            {
                subarrayLength = increasingSubArray[i] + decreasingSubArray[i] - 1;
                beginIndex = i - increasingSubArray[i] + 1;
                endIndex = i + decreasingSubArray[i] - 1;
            }
        }
        int[] bitonicSubarray = new int[subarrayLength];
        System.out.println("The longest bitonic subarray is:");
        for(int i=beginIndex;i<=endIndex;i++){
            bitonicSubarray[i-beginIndex] = myArray[i];
            System.out.print( bitonicSubarray[i-beginIndex]+" ");
        }
        System.out.println();
        // print longest bitonic sub-array
        System.out.println("The length of longest bitonic sub-array is " + subarrayLength);
        System.out.println("The longest bitonic subarray is between " + beginIndex + " and " + endIndex + " positions in myArray");

    }

    public static void main(String[] args)
    {
        int[] A = { -1, 8, 4, 13, 17, 5, 1, 19, 4};

        findBitonicSubarray(A);
    }
}
